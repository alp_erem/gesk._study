import paho.mqtt.client as mqtt
import time
import requests
import json
import sqlite3
import datetime 

topic = "drone_data"
client = mqtt.Client("gesk_listener1234") 
client.connect('159.146.47.86', 39039)

sql_con = sqlite3.connect("drone_data.db")
cursor = sql_con.cursor()
#global team_number, latitude, longitude, altitude, pitch, yaw, roll, velocity, battery, hour, minute, second, milisecond

def table_create():
    cursor.execute("""CREATE TABLE IF NOT EXISTS drone (time REAL, 
                    team_number BLOB, latitude BLOB, longitude BLOB, 
                    altitude BLOB, pitch BLOB, yaw BLOB, roll BLOB, 
                    velocity BLOB, battery BLOB, hour BLOB, minute BLOB, 
                    second BLOB, milisecond BLOB)""")
    sql_con.commit()
    #sql_con.close()
    
def table_add(team_number, latitude, longitude, altitude, pitch, yaw, roll, velocity, battery, hour, minute, second, milisecond):
    date = datetime.now()
    cursor.execute("INSERT INTO drone VALUES ('{}','{}','{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(str(date.time())[0:8], team_number, latitude, longitude, altitude, pitch, yaw, roll, velocity, battery, hour, minute, second, milisecond))
    sql_con.commit()
    #en son tablo kapatılacak 

def send_telegram(data):
    token = "5069873430:AAGdv57g1AMPc0KvY4nHktxHwiTfLc_u-KA"
    chat_id = "1812779005" #ben
    chat_id2 = "-636445881" #eren abi ve ben (grup)
    url_req = "https://api.telegram.org/bot"+ token +"/sendMessage" + "?chat_id=" + chat_id + "&text=" + str(data)
    result = requests.get(url_req)
    print(result.json())

def on_message(client, userdata, msg):
    data = msg.payload.decode("utf-8")
    data = json.loads(data)
    print(data["takim_numarasi"],data["IHA_enlem"],data["IHA_boylam"],data["IHA_irtifa"],data["IHA_dikilme"],data["IHA_yonelme"],data["IHA_yatis"],data["IHA_hiz"],data["IHA_batarya"],data["GPSSaati"]["saat"],data["GPSSaati"]["dakika"],data["GPSSaati"]["saniye"],data["GPSSaati"]["milisaniye"])
    table_add(data["takim_numarasi"],data["IHA_enlem"],data["IHA_boylam"],data["IHA_irtifa"],data["IHA_dikilme"],data["IHA_yonelme"],data["IHA_yatis"],data["IHA_hiz"],data["IHA_batarya"],data["GPSSaati"]["saat"],data["GPSSaati"]["dakika"],data["GPSSaati"]["saniye"],data["GPSSaati"]["milisaniye"])
    send_telegram(data)



client.loop_start()
for i in range(20):
    client.subscribe(topic)
    client.on_message = on_message
    time.sleep(1)
client.loop_stop()
sql_con.close()