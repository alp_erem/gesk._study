import paho.mqtt.client as mqtt
import time
from dronekit import *
import datetime
import json

iha = connect("/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0001-if00-port0", baud=57600, wait_ready=True, timeout=120)

topic = "drone_data"
client = mqtt.Client("gesk_listener1234")  # Create instance of client with client ID "digi_mqtt_test"
client.username_pw_set(username="owntracks", password="orangepizero")
client.connect('159.146.47.86', 39039)
for i in range(1000):
    latitude = iha.location.global_relative_frame.lat
    longitude = iha.location.global_relative_frame.lon
    altitude = iha.location.global_relative_frame.alt
    pitch = iha.attitude.pitch
    yaw = iha.attitude.yaw
    roll = iha.attitude.roll
    velocity = iha.airspeed  #hız
    battery_voltage = int(iha.battery.voltage) #iha.iha.battery["voltage"]  # pil voltajı
    battery = 100-((16.8-battery_voltage)*100/8)

    '''@iha.on_message('SYSTEM_TIME')
                def listener(self, name, message):
                    global hour, minute, second, milisecond
                    unix_time = (int)(message.time_unix_usec / 1000000)
                    hour = datetime.datetime.fromtimestamp(unix_time).hour - 3
                    minute = datetime.datetime.fromtimestamp(unix_time).minute
                    second = datetime.datetime.fromtimestamp(unix_time).second
                    milisecond = datetime.datetime.fromtimestamp(unix_time).microsecond / 1000
            
                listener
            '''
    telemetriler = {
                    "takim_numarasi": 1,#
                    "IHA_enlem": latitude,
                    "IHA_boylam": longitude,
                    "IHA_irtifa": altitude,
                    "IHA_dikilme": pitch,
                    "IHA_yonelme": yaw,
                    "IHA_yatis": roll,
                    "IHA_hiz": velocity,
                    "IHA_batarya": battery,
                    "GPSSaati": {
                        "saat": 1,
                        "dakika": 1,
                        "saniye": 1,
                        "milisaniye": 1
                    }
                }

    mqtt_msg = json.dumps(telemetriler)

    client.publish(topic,mqtt_msg)
    time.sleep(1) # saniye
    print("true")
    #print(telemetriler["takim_numarasi"],telemetriler["IHA_enlem"],telemetriler["IHA_boylam"],telemetriler["IHA_irtifa"],telemetriler["IHA_dikilme"],telemetriler["IHA_yonelme"],telemetriler["IHA_yatis"],telemetriler["IHA_hiz"],telemetriler["IHA_batarya"],telemetriler["GPSSaati"]["saat"],telemetriler["GPSSaati"]["dakika"],telemetriler["GPSSaati"]["saniye"],telemetriler["GPSSaati"]["milisaniye"])