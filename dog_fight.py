import threading 
import time
from queue import Queue

class soldier:
	def __init__(self, name, health, damage):
		self.name = name
		self.health = health
		self.damage = damage

	def hit(self, y, q):
		while True:
			self.health -= y.damage
			print("{} hit the {}".format(self.name, y.name))
			print("health of the {} is {}".format(self.name, self.health))
			data1=self.health
			q.put(data1)
			time.sleep(0.3)
			data2 = q.get()
			print("b'nin canı",data2)
			if self.health <= 0:
				print("{} was died!".format(self.name))
				break
			if data2 <= 0:
				print("*************************************")
				break

	def hit2(self, y, in_q):
		while True:
			self.health -= y.damage
			print("{} hit the {}".format(self.name, y.name))
			print("health of the {} is {}".format(self.name, self.health))
			data2= self.health
			q.put(data2)
			time.sleep(0.3)
			data1 = q.get()
			print("a'nın canı",data1)
			if self.health <= 0:
				print("{} was died!".format(self.name))
				break
			if data1 <= 0:
				print("*************************************")
				break
	


for i in range(10):	
	p1 = soldier("a", 150, 30)
	p2 = soldier("b", 350, 10)

	q = Queue()
	t1 = threading.Thread(target = p1.hit, args = (p2, q))
	t2 = threading.Thread(target = p2.hit2, args = (p1, q))

	t1.start()
	t2.start()

	t1.join()
	t2.join()

	print("finished\n\n")
